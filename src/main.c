#define _DEFAULT_SOURCE
#include "mem.h"
#include "mem_internals.h"

struct block_header* initBlock(size_t size){
    static int16_t counter = 1;
    int16_t * block = _malloc(size);
    if(block) *block = counter++;
    return (struct block_header *) ((uint8_t *) block - offsetof(struct block_header, contents));
}

void free_block(struct block_header* blockHeader){
    _free(blockHeader->contents);
}

int main() {
    void* heap = heap_init(1000);
    {
        printf("\nTest1 1: Normal allocation\n");
        struct block_header* blockHeader1 = initBlock(1000);
        if(blockHeader1->capacity.bytes == 1000 && !blockHeader1->is_free && blockHeader1 == HEAP_START){
            if(blockHeader1->next != NULL && blockHeader1->next->is_free && blockHeader1->next->next == NULL){
                printf("Successful !!\n");
            } else printf("Next Block Invalid\n");
        } else printf("Block Invalid\n");
        debug_heap(stdout,heap);
        free_block(blockHeader1);
    }
    {
        printf("\nTest 2: Freeing one block from several blocks");
        struct block_header* blockHeader1 = initBlock(1000);
        struct block_header* blockHeader2 = initBlock(1000);
        struct block_header* blockHeader3 = initBlock(1000);
        printf("\nBefore freeing\n");
        debug_heap(stdout,heap);
        free_block(blockHeader2);
        printf("\nAfter freeing seconf block");
        debug_heap(stdout,heap);
        if(blockHeader2->is_free == true && !blockHeader1->is_free && !blockHeader3->is_free) printf("Successful!!\n");
        else printf("Unsuccessfully free second block\n");
        free_block(blockHeader3);
        free_block(blockHeader1);
    }
    {
        printf("\nTest 3: Freeing two blocks from several blocks");
        struct block_header* blockHeader1 = initBlock(1000);
        struct block_header* blockHeader2 = initBlock(1000);
        struct block_header* blockHeader3 = initBlock(1000);
        struct block_header* blockHeader4 = initBlock(1000);
        struct block_header* blockHeader5 = initBlock(1000);
        printf("\nBefore freeing\n");
        debug_heap(stdout,heap);
        free_block(blockHeader2);
        free_block(blockHeader4);
        printf("\nAfter freeing second block");
        debug_heap(stdout,heap);
        if(blockHeader4->is_free && blockHeader2->is_free
        && !blockHeader1->is_free && !blockHeader3->is_free && !blockHeader5->is_free) printf("Successful!!\n");
        else printf("Unsuccessfully free second block\n");
        free_block(blockHeader1);
        free_block(blockHeader3);
        free_block(blockHeader5);
    }
    {
        printf("\nTest 4: The new region expands the old one\n");
        struct block_header* blockHeader1 = initBlock(1000);
        printf("Heap before growing\n");
        debug_heap(stdout,heap);
        size_t old_size_heap = size_from_capacity(blockHeader1->capacity).bytes + size_from_capacity(blockHeader1->next->capacity).bytes;
        struct block_header* blockHeader2 = initBlock(8000);
        printf("Heap after growing\n");
        debug_heap(stdout,heap);
        size_t new_size_heap = size_from_capacity(blockHeader1->capacity).bytes + size_from_capacity(blockHeader2->next->capacity).bytes +
                size_from_capacity(blockHeader2->capacity).bytes;
        if(new_size_heap > old_size_heap && blockHeader2 != NULL) printf("Successful !!\n"); else printf("Unsuccessful !!\n");
        free_block(blockHeader1);
        free_block(blockHeader2);
    }
    {
        printf("\nTest 5: The new region doesn't expand the old one\n");
        struct block_header* blockHeader1 = initBlock(1000);
        void* end_of_heap = (void*) (blockHeader1->next->contents + blockHeader1->next->capacity.bytes);
        void* res = mmap(end_of_heap, REGION_MIN_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS  | MAP_FIXED, -1 ,0);

        if(res == MAP_FAILED){
            printf("unable to proceed test\n");
        }
        struct block_header* blockHeader2 = initBlock(32000);
        if(blockHeader2->contents){
            *blockHeader2->contents = 0xF;
        } else {
            printf("failed, unable to allocate new block\n");
        }
        printf("Successfully !! \n");
        debug_heap(stdout,heap);
        free_block(blockHeader1);
        free_block(blockHeader2);
    }

}
